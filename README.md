
# Installing unsigned addons on firefox.

## TL;DR

1. Make sure `xpinstall.signatures.required` is set to `false`.

2. Run

     ./firefox-enable-unsigned-addons.sh /path/to/firefoxInstallDir/omni.ja

3. After firefox updates, you obviously need to re-run the script in order to patch the new version.\
   But you may also need to delete the `<PROFILE>/startupCache/` folder (for each profile).


## Long version

[Mozilla requires all extensions to be signed by Mozilla in order for them to be installable in Release and Beta versions of Firefox.](https://wiki.mozilla.org/Add-ons/Extension_Signing).

On the Release and Beta versions, you can temporarily test an addon, but it disappears on restart:

  - Go to `about:debugging#addons` (in the url bar)
  - Check "Enable add-on debugging"
  - Click on "Load Temporary Add-on"

Installing unsigned addons is possible on the nightly and DeveloperEdition versions.
It's also possible in [unbranded](https://wiki.mozilla.org/Add-ons/Extension_Signing#Unbranded_Builds) versions of firefox.

Alternatively, you can hack the Release and Beta versions of firefox in
order to enable the installation of unsigned addons. Note that this is
a "hack", not "customization", so you'll need to repeat it after each
firefox update.

The `firefox-enable-unsigned-addons.sh` shell script automates the hack.

To do it manually,

- Step 1/

  You need to edit `omni.ja` (you can find it at the root of the firefox
  installation folder) with some binary-friendly editor (NB: vim/emacs
  both work fine).

  Note: make a backup of before editing the file.

  Then search for "MOZ_REQUIRE_SIGNING", and change
  ~~~
    MOZ_REQUIRE_SIGNING:
  //@line 235 "/builds/worker/workspace/build/src/toolkit/modules/AppConstants.jsm"
    true,
  //@line 239 "/builds/worker/workspace/build/src/toolkit/modules/AppConstants.jsm"
  ~~~
  into
  ~~~
    MOZ_REQUIRE_SIGNING:
  //@line 235 "/builds/worker/workspace/build/src/toolkit/modules/AppConstants.jsm"
   false,
  //@line 239 "/builds/worker/workspace/build/src/toolkit/modules/AppConstants.jsm"
  ~~~

  IMPORTANT:
    Please note that no character was added: "`␣␣true`" is replaced by "`␣false`".
    You have one space less. It's important, as firefox will crash at startup otherwise.

  Note: depending of the version of firefox, the line numbers can change.

- Step 2/

  Restart firefox. If firefox crashes, read the paragraph just above.

- Step 3/

  Toggle the `xpinstall.signatures.required` preference in `about:config`
  (set it to `false`).

  You can now enjoy unsigned addons... until next firefox update.


Note: the trick from 2016 referenced at <https://www.ghacks.net/2016/08/14/override-firefox-add-on-signing-requirement/>
does not work any more in recent versions of firefox.
