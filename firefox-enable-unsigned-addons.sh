
# This shell script patches firefox/omni.ja in order to enable unsigned extensions.


function show-help () {
    printf '%s' "
This script patches <firefoxInstallDir/omni.ja>,
in order to set 'MOZ_REQUIRE_SIGNING' to false.
Note: it's safe to run this script more than once.

Usage:
  ./firefox-enable-unsigned-addons.sh firefoxInstallDir[/omni.ja]

Important:
  if firefox auto-updates, you also need to delete the
  '<PROFILE>/startupCache/' folder (for each profile).

Note:
  in 'about:config', you also need to set
  'xpinstall.signatures.required' to 'false'.
"
}


function show-info () {
    printf '%s' "
Notes:

• in 'about:config',
    - check that 'xpinstall.signatures.required' is false.
    - check that 'app.update.auto'               is false (optional).

• When Firefox updates

  1. You need to patch 'omni.ja' every time you upgrade firefox.

     => i.e. re-run this script.

  2. If firefox auto-updates, patching 'omni.ja' is not enough.
     To fix this,

     => you have to delete the '<PROFILE>/startupCache/' folder
        (for each profile).
"
}


function patch-omnija () {

    local OMNI_JA=${1:-omni.ja}


    # check arguments

    if [[ ! -f "${OMNI_JA}" && -f "${OMNI_JA%/}/omni.ja" ]]; then
            OMNI_JA="${OMNI_JA%/}/omni.ja"
    fi
    if [[ ! -f "${OMNI_JA}" ]]; then
        if [[ -z "$1" ]]; then  # run with no argument => show help
            show-help
            return 0
        else
            echo "File '${OMNI_JA}' does not exist. Exiting..."
            return 1
        fi
    fi

    echo "Found '${OMNI_JA}'."
    echo "Processing ..."
    echo ""

    # check for required tools

    local HAS_GREP HAS_CUT HAS_DD  # (mantatory)
    local HAS_BBE                  # (optional)
    which grep > /dev/null && HAS_GREP=true || HAS_GREP=false
    which cut  > /dev/null && HAS_CUT=true  || HAS_CUT=false
    which dd   > /dev/null && HAS_DD=true   || HAS_DD=false
    which bbe  > /dev/null && HAS_BBE=true  || HAS_BBE=false

    local -a MISSING_TOOLS=()
    ${HAS_GREP} || MISSING_TOOLS+=( "grep" )
    ${HAS_CUT}  || MISSING_TOOLS+=( "cut" )
    ${HAS_DD}   || MISSING_TOOLS+=( "dd" )
    if [[ ${#MISSING_TOOLS[@]} -gt 0 ]]; then
        echo "Missing required tools: ${MISSING_TOOLS[*]}."
        return 1
    fi


    # We are looking for the following block of code

    # (1) |   MOZ_REQUIRE_SIGNING:
    # (2) |//@line 280 "$SRCDIR/toolkit/modules/AppConstants.jsm"
    # (3) |  true,
    # (4) |//@line 284 "$SRCDIR/toolkit/modules/AppConstants.jsm"

    # locate 1st line ("MOZ_REQUIRE_SIGNING" string)
    local OFFSET1=$(grep --text --line-number --byte-offset --only-matching --max-count=1 'MOZ_REQUIRE_SIGNING:' "${OMNI_JA}" | cut -d: -f1,2)
    [[ -z "${OFFSET1}" ]] && { echo "Error: Unexpected omni.ja data (Step 1). Exiting..."; return 1; }
    local OFFSET1_LINE=$(printf '%s' "${OFFSET1}" | cut -d: -f1)
    local OFFSET1_BYTE=$(printf '%s' "${OFFSET1}" | cut -d: -f2)

    # locate 2nd line (first "toolkit/modules/AppConstants.jsm" string)
    local OFFSET2=$(dd if="${OMNI_JA}" bs=100M iflag=skip_bytes skip=${OFFSET1_BYTE} | grep --text --line-number --byte-offset --only-matching --max-count=1 '^//@line.*toolkit/modules/AppConstants.jsm' | cut -d: -f1,2)
    [[ -z "${OFFSET2}" ]] && { echo "Error: Unexpected omni.ja data (Step 2). Exiting..."; return 1; }
    local OFFSET2_LINE=$(printf '%s' "${OFFSET2}" | cut -d: -f1)
    local OFFSET2_BYTE=$(printf '%s' "${OFFSET2}" | cut -d: -f2)

    # locate 3rd line (the "  true," line)
    local OFFSET3=$(dd if="${OMNI_JA}" bs=100M iflag=skip_bytes skip=$(( OFFSET1_BYTE + OFFSET2_BYTE )) | grep --text --line-number --byte-offset --only-matching --max-count=1 '^  true,$' | cut -d: -f1,2)
    local OFFSET3_LINE=$(printf '%s' "${OFFSET3}" | cut -d: -f1)
    local OFFSET3_BYTE=$(printf '%s' "${OFFSET3}" | cut -d: -f2)

    # locate 3rd line when already patched (i.e. with 'true' replaced by 'false')
    local OFFSET4=$(dd if="${OMNI_JA}" bs=100M iflag=skip_bytes skip=$(( OFFSET1_BYTE + OFFSET2_BYTE )) | grep --text --line-number --byte-offset --only-matching --max-count=1 '^ false,$' | cut -d: -f1,2)
    local OFFSET4_LINE=$(printf '%s' "${OFFSET4}" | cut -d: -f1)
    local OFFSET4_BYTE=$(printf '%s' "${OFFSET4}" | cut -d: -f2)

    if [[ ${OFFSET2_LINE} -eq 2 && ${OFFSET3_LINE} -eq 2 ]]; then  # expected condition
        if ${HAS_BBE}; then
            local OMNI_JA_PATCHED="${OMNI_JA}.patched-bbe"
            bbe --block=$(( OFFSET1_BYTE + OFFSET2_BYTE + OFFSET3_BYTE )):6 --expression='r 1 false' "${OMNI_JA}" > "${OMNI_JA_PATCHED}"
        else
            local OMNI_JA_PATCHED="${OMNI_JA}.patched-dd"
            dd if="${OMNI_JA}" bs=100M iflag=count_bytes count=$(( OFFSET1_BYTE + OFFSET2_BYTE + OFFSET3_BYTE + 1 )) > "${OMNI_JA_PATCHED}"
            printf 'false,' >> "${OMNI_JA_PATCHED}"
            dd if="${OMNI_JA}" bs=100M iflag=skip_bytes skip=$(( OFFSET1_BYTE + OFFSET2_BYTE + OFFSET3_BYTE + 7 )) >> "${OMNI_JA_PATCHED}"
        fi

        mv "${OMNI_JA}" "${OMNI_JA}.orig"     # make a copy of the original omni.ja
        mv "${OMNI_JA_PATCHED}" "${OMNI_JA}"  # and replace it by the patched version

        printf '\n\n%s\n\n%s\n' "Patched! (${OMNI_JA})" "$(show-info)"

        return 0
    elif [[ ${OFFSET2_LINE} -eq 2 && ${OFFSET4_LINE} -eq 2 ]]; then  # already patched
        printf '\n%s\n\n%s\n' "Already patched (${OMNI_JA})." "$(show-info)"
        return 0
    else
        echo "Fail to patch ${OMNI_JA}."
        return 1
    fi
}


# main

if [[ "$1" == "-h" || "$1" == "--help" ]]; then
    show-help
else
    patch-omnija "$@"
fi
